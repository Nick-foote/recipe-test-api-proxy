FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer='nick-foote'

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

COPY ./entrypoint.sh /entrypoint.sh

# Serve files through nginx proxy
# touch creates an empty file
# chown user:group
# +x makes file executable
RUN mkdir -p /vol/static && \
    chown 755 /vol/static && \
    touch /etc/nginx/conf.d/default.conf && \
    chown nginx:nginx /etc/nginx/conf.d/default.conf && \
    chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]
