{* 
    tpl = template 
    alias = location where to server the files from
    Handles the server in the order given below. Static first, then the next location
    location / = anything that doesn't match static, everything else
    10M = MB
*}
server {
    listen                      ${LISTEN_PORT};


    location /static {
        alias                   /vol/static;
    }

    location / {
        uwsgi_pass              ${APP_HOST}:${APP_PORT};
        include                 /etc/nginx/uwsgi_params;
        client_max_body_size    10M;
    }
}